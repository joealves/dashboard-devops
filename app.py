from flask import Flask, render_template
from modulos.bdocker import docker
from modulos.bjenkins import jenkins
from modulos.bgitlab import gitlab
import logging

app = Flask(__name__)
app.register_blueprint(docker)
app.register_blueprint(jenkins)
app.register_blueprint(gitlab)
logging.basicConfig(
    filename='app.log',
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s ] %(name)s\n" +
    "[ %(funcName)s ] [%(filename)s, %(lineno)s] %(message)s",
    datefmt="[ %d-%m-%Y %H:%M:%S ]"
)


@app.route('/')
def index():
    logging.info("Mensagem de info")
    return render_template('index.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
